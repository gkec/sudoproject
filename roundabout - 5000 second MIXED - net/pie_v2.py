import xml.sax
import re
from itertools import cycle
import random

Edges = {} 
Nodes = {}
Routes = {}

class node:
	def __init__(self , nodeid , x , y):
		self.nodeID = nodeid 
		self.xcord = x 
		self.ycord = y
		self.startEdgeIDs = []
		self.endEdgeIDs = []

	def addStartEdge(self,edgeID):
		self.startEdgeIDs.append(edgeID)

	def addEndEdge(self,edgeID):
		self.endEdgeIDs.append(edgeID)

	def hasEdge(self,edgeID):
		for id in self.startEdgeIDs:
			if id == edgeID:
				return -1  # Indicate is the starting node of an edge
		for id in self.endEdgeIDs:
			if id == edgeID:
				return 1   # Indicate is the ending node of an edge
		return 0

	def route(self, current_route):
		# For each edge where the node is the start point, create a route section to
		# each end node
		# Note that this is recursive as it is repeatedly called for each Node until
		# the end is found
		start_ids = len(self.startEdgeIDs)
		if( start_ids == 0 ):  # One way route out
			current_route.finish()
			return 

		for eid in self.startEdgeIDs:
			new_route = route(current_route)
			edge = Edges[eid]
			end_node = Nodes[edge.toNodeID]

			if(new_route.addEdge(eid)):
				# Get end node to add more route segments
				end_node.route(new_route)
			else:
				# Route goes in a circle or back on itself -- abandon route
				if((start_ids == 1) and (len(self.endEdgeIDs) == 1 )) : # Check for node with one way in and one way out
					# Check that the way out is the way back rather than a one way edge going on to somewhere else
					previous_edge = Edges[self.endEdgeIDs[0]]
					if( previous_edge.toNodeID == edge.fromNodeID ):
						new_route.finish()
						return
				else: 
					dummy = 0 # Place holder for alternative handling code

	def dummy():
		return

	def __str__(self):
		return "Node ID:" + self.nodeID \
		+ " X Cord:" + self.xcord \
		+ " Y Cord:" + self.ycord

	def xml(self):
		return "<node id=" + self.nodeID \
		+ " x=" + self.xcord \
		+ " y=" + self.ycord + "/>" 

class edge:
	def __init__(self ,id , fromnodeid , tonodeid):
		self.edgeid = id
		self.fromNodeID = fromnodeid
		self.toNodeID = tonodeid
		self.type = "a"
		self.roaddistance = -1

	def __str__(self):
		return "Edge ID:" + self.edgeid \
		+ " From:" + self.fromNodeID \
		+ " To:" + self.toNodeID \
		+ " Distance:" + str(self.distance()) 

	def xml(self):
		return "<edge id=" + self.edgeid \
		+ " from=" + self.fromNodeID \
		+ " to=" + self.toNodeID \
		+ " type=" + self.type + "/>"

	def distance(self):
		if self.roaddistance < 0:
			startNode = Nodes[self.fromNodeID]
			if startNode == None:
				return -1 
			startnodex = int(startNode.xcord)
			startnodey = int(startNode.ycord)
			#define end node and get coordinates
			endNode = Nodes[self.toNodeID]
			if endNode == None:
				return -1 
			endnodex = int(endNode.xcord)
			endnodey = int(endNode.ycord)
			#distance calculation = (dx^2 + dy^2)^1/2
			self.roaddistance = ((abs(startnodex - endnodex)) ** 2 + (abs(startnodey - endnodey)) ** 2) ** 0.5
		return self.roaddistance 

class routeplan():
		def __init__(self, edgepicked):
				self.startnode = edgepicked
		def __str__(self):
				return "start Edge:" + self.startnode\

class nodeHandler(xml.sax.ContentHandler):
		def startElement(self, tag, attributes):
			#Pull data from xml documents and store in dictionarys
			if tag == "node":
					nodeID = attributes["id"]
					xcord = attributes["x"]
					ycord = attributes["y"]
					n = node(nodeID , xcord , ycord)
					Nodes[nodeID] = n

class edgeHandler(xml.sax.ContentHandler):
		def startElement(self, tag, attributes):
			#Pull data from xml documents and store in dictionarys
			if tag == "edge": 
					edgeID = attributes["id"]
					toNodeID = attributes["to"]
					fromNodeID = attributes["from"]
					e = edge(edgeID , fromNodeID , toNodeID)
					Edges[edgeID] = e
					
class route():
	# The Objective of this part is to the build the "S1 E2 S5 E7 S9 S12" part.
	routeID = 0 									
	def __init__(self,old_route):
		route.routeID = route.routeID + 1
		self.id = route.routeID
		self.EdgeIDs = []
		self.usecount = 0
		if old_route:
		    self.distance = old_route.distance 
		else:
			self.distance = 0 
		# Copy edge IDs
		if old_route:
			for e in old_route.EdgeIDs:
				self.EdgeIDs.append(e) 

	def addEdge(self, edgeID):
		# Check that end of edge node has not been visited before
		edge = Edges[edgeID]
		endNodeID = edge.toNodeID
		for eid in reversed( self.EdgeIDs ): # Do reverse search as local loops more likely 
			prevEdge = Edges[eid]
			if prevEdge.fromNodeID == endNodeID:
				return False
		self.EdgeIDs.append(edgeID)
		self.distance += edge.distance()
		return True

	# Finish off by adding to Routes list
	def finish(self):
		Routes[int(self.id)] = self # Force to int so we can sort numerically if we want to
	

	def compareRouteEdges(self, startEdgeIDs, endEdgeIDs):
		#ensure route is quickroute
		for A in node.startEdgeIDs:
			for Z in node.endEdgeIDs:
				if A and Z in Routes:
					return True
					return "Start edge: "+ A + "End Edge: " + Z +"Has distance: "+str(self.distance) 
				else: 
					return False
					return "no value - if"

	def __str__(self):
		rstr = "Route ID: " + str(self.id)
		for eid in self.EdgeIDs:
			rstr += " " + eid  
		rstr += " Total Disance: " + str(self.distance)
		return rstr

	def xml(self):
		RouteXML= '<route id="route' + str(self.id) + '" edges="'
		for eid in self.EdgeIDs:
				RouteXML += " " + eid
		RouteXML += '"/>'
		#RouteXML +=' use count: '+str(self.usecount)
		return RouteXML

	def getStartEdgeID(self):
		return self.EdgeIDs[0]

	def getEndEdgeID(self):
		return self.EdgeIDs[-1]
	
	def getrouteID(self):
		return self.id

	def findQuickestRoutes(self):
		shortest_route = None
		sorted_routes = sorted(Routes.items()) 
		for first_route_id, first_route in sorted_routes:
			shortest_route = first_route
			for next_route_id , next_route in sorted_routes:
				if( shortest_route.compareRouteEdges( next_route.getStartEdgeID() , next_route.getEndEdgeID())):
                	# Same end points
					if( shortest_route.distance > next_route.distance ):
						# Remove longer route from list
						if( int(shortest_route.distance) > 1.1*int((next_route.distance)) ): # LEEWAY - NOT FUNCTIONAL
							sorted_routes.pop(shortest_route.id) 
                    	# Setup new shortest route
						shortest_route = next_route                            

if (__name__ == "__main__"):
	# create an XMLReader
	parser = xml.sax.make_parser()

	# turn off namespaces
	parser.setFeature(xml.sax.handler.feature_namespaces, 0)

	# override the default ContextHandler
	nHandler = nodeHandler()
	eHandler = edgeHandler()

	# Process nodes first
	parser.setContentHandler(nHandler)
	parser.parse("round.nod.xml")

	# Then edge
	parser.setContentHandler(eHandler)
	parser.parse("round.edg.xml")

	#for eid in Edges:
	#	print Edges[eid]

	#for nid in Nodes:
	#	print Nodes[nid]


	# Add edge IDs to Nodes so each node knows which edges are associated with it
	for eid in Edges:
		e = Edges[eid]
		Node = Nodes[e.fromNodeID] 
		Node.addStartEdge(eid)
		Node = Nodes[e.toNodeID]
		Node.addEndEdge(eid)
	
	# select nodes and determine routes
	for n in Nodes:
		#only select start nodes
		node = Nodes[n]
		if node.nodeID.startswith('A'):
			new_route = route(None)
			# Use starting node to construct routes to all end nodes
			node.route(new_route)

	#efficient routes
	def sortRoutes():
		shortest_routes = {}
		sr = sorted(Routes, key=lambda k: (Routes[k].getStartEdgeID(), Routes[k].getEndEdgeID(), Routes[k].distance))
		shortestdistance = None
		first_route = Routes[sr[0]];
		for route1_id in sr:
			route1 = Routes[route1_id]
			if(( route1.EdgeIDs[0] == first_route.EdgeIDs[0]) and (route1.EdgeIDs[-1] == first_route.EdgeIDs[-1])):
				if( route1.distance == first_route.distance ):
					shortest_routes[route1.id] = route1 
			else:
				first_route = route1 
				shortest_routes[route1.id] = route1

		sortRoutes.sr = sorted(shortest_routes, key=lambda k: (shortest_routes[k].getStartEdgeID(), shortest_routes[k].getEndEdgeID(), shortest_routes[k].distance))
		sortRoutes.rand = sorted(shortest_routes, key=lambda k: (shortest_routes[k].getEndEdgeID()))

		#print len(Routes)
		#print len(sr)
		#print len(sortRoutes.sr)
		#print len(shortest_routes)

		for r in sortRoutes.sr :
			print Routes[r].xml()
		return shortest_routes


	sortRoutes()

	def journey():
		# VARIABLES ARE HERE
		cars = 20000 		#total of x cars
		CPS = 2				#cars per second
		timer = 0
		rate = CPS**-1
		routes = iter(sortRoutes.rand)
		route_cycle = cycle(routes)
		while cars != 0:
			timer = timer + rate
			route = route_cycle.next()
			print '<vehicle depart="'+str(int(round(timer)))+'" id="veh'+str(cars)+'" route="route'+str(route)+'" type="CarA" />'
			cars = cars -1
	journey()







		#while cars != 0:
		#	if CPS >= 1:
		#		while release < 1:
		#			route = route_cycle.next()
		#			print '<vehicle depart="'+str(timer)+'" id="veh'+str(cars)+'" route="route'+str(route)+'" type="CarA" />'
		#			release =  release + rate
		#			cars = cars -1
		#		timer = timer + 1
		#		release = 0



		#	if CPS < 1:
		#		while release < rate:
		#			timer = timer + 1
		#			release = release + 1
		#			route = route_cycle.next()
		#			print '<vehicle depart="'+str(timer)+'" id="veh'+str(cars)+'" route="route'+str(route)+'" type="CarA" />'
		#			cars = cars -1
		#		release = 0


	#for eid in Edges:
	#	print Edges[eid]

	#for nid in Nodes:
	#	print Nodes[nid]


	#  for e in Edges:
	#     print e.xml()

	#  for n in Nodes:
	#     print n.xml()

	# Handler.distance()

	#if (Handler.maxroads != 0):
	#	print Handler.maxroads
	#else:
	#	print "No max road"

	#if (Handler.marounds != 0):
	#	print Handler.maxjunctions
	#else:
	#	print "No max junction"


		
		#for r in sr :
		#	print Routes[r]

		#	#aim: <route id="route01" edges="S1 E2 S5 E7 S9 S12"/>
		#	RouteXML= '"<route id="route'+str(Routes.routeID)+'"edges="'
		#	for eid in Routes.EdgeIDs:
		#		RouteXML += " " + eid
		#	RouteXML += '"/>'
		#	return RouteXML





		#return '<route id="route' + self.edgeid \
		#+ '"edges="' + self.fromNodeID \
		#+ ' to=' + self.toNodeID \
		#+ ' type=' + self.type + '/>'









		#print Routes[1].getrouteID()
		#for r in CarRoutes :
		#	print Routes[r].getrouteID()
		#	print Routes[r]
		#	print  str(Routes[r].id) + " to " + Routes[r].getStartEdgeID() + " to " + Routes[r].getEndEdgeID()
		#	print Routes[r].usecount
	##############################
		#	while cars != 0:
		#	if CPS >= 1:
		#		while release < 1:

				
					#print '<vehicle depart="'+str(timer)+'" id="veh'+str(cars)+'" route="'+str(routeused)+'" type="CarA" />'
		#			release =  release + rate
		#			cars = cars -1
		#		timer = timer + 1
		#		release = 0
######################################



					#CarRoutes = sorted(Routes, key=lambda k: (Routes[k].usecount))
					#for r in CarRoutes: 
						#print '<vehicle depart="'+str(timer)+'" id="veh'+str(cars)+'" route="'+str(Routes[r].id)+'" type="CarA" />'
						#Routes[r].usecount = Routes[r].usecount + 1
						#print Routes[r].usecount
						#release =  release + rate
						#cars = cars -1

					#routeused = Routes[k].getrouteID()
					#x = x+1



	#for id, route in sorted(Routes.items()):
	#	print route
	#print len (Routes)


	#myroute = Routes[id]
	#print myroute.xml()

	#for r in Routes:
	#	print route.compareRouteEdges(node.startEdgeIDs, node.endEdgeIDs)

	#route.findQuickestRoutes(self)






