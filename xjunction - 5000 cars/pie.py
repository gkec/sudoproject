import xml.sax
import re

Edges = [] 
Nodes = []
Distances = []
CurrentRoute = []
Routes = []
NodesAvailable = []
EdgesAvailable = []

class node:
	def __init__(self , _id , x , y ):
		self.nodeid = _id 
		self.xcord = x 
		self.ycord = y

	def __str__(self):
		return "Node ID:" + self.nodeid \
		+ " X Cord:" + self.xcord \
		+ " Y Cord:" + self.ycord

	def xml(self):
		return "<node id=" + self.nodeid \
		+ " x=" + self.xcord \
		+ " y=" + self.ycord + "/>" 

class edge:
	def __init__(self ,id , _from , _to):
		self.edgeid = id
		self.fromnode = _from
		self.tonode = _to
		self.type = "a"

	def __str__(self):
		return "Edge ID:" + self.edgeid \
		+ " From:" + self.fromnode \
		+ " To:" + self.tonode

	def xml(self):
		return "<edge id=" + self.edgeid \
		+ " from=" + self.fromnode \
		+ " to=" + self.tonode \
		+ " type=" + self.type + "/>"

class dist():
        def __init__(self, startnode, endnode, EdgeDistance):
                self.SNode = startnode
                self.Enode = endnode
                self.roaddistance = EdgeDistance
        def __str__(self):
                return "from:" + self.SNode.nodeid \
                + " To:" + self.Enode.nodeid \
                + " Has a distance of:" + str(self.roaddistance)

class routeplan():
        def __init__(self, edgepicked):
                self.startnode = edgepicked
        def __str__(self):
                return "Routes: " + self.startnode\

class routebuild():
        def __init__(self, edgepicked):
                self.startnode = edgepicked
        def __str__(self):
                return "Routes: " + self.startnode\

class calc( xml.sax.ContentHandler ):
        def __init__(self):
                self.CurrentData = ""
                self.nodeid = ""
                self.xcord = ""
                self.ycord = ""
                self.edgeid = ""
                self.fromnode = ""
                self.tonode = ""
                self.maxjunctions = 0
                self.maxroads = 0
                self.currentnode = ""
                self.currentroad = ""
                self.selectededgeid = ""
                self.selectedfrom = ""
                self.selectedto = ""
                self.selectednodeid1 = ""
                self.selectednodex1 = ""
                self.selectednodey1 = ""
                self.selectednodeid2 = ""
                self.selectednodex2 = ""
                self.selectednodey2 = ""
                self.edgedistance = ""
                self.nopepicked = ""
                self.nextnode = ""
                self.newnode = ""
                self.nodeused = ""

        def startElement(self, tag, attributes):
            #Pull data from xml documents and store in dictionarys
                self.CurrentData = tag
                if tag == "node":
                        nodeid = attributes["id"]
                        xcord = attributes["x"] 
                        ycord = attributes["y"]
                        n = node( nodeid , xcord , ycord )
                        Nodes.append(n)

                if tag == "edge": 
                        edgeid = attributes["id"]
                        _to = attributes["to"] 
                        _from = attributes["from"]
                        e = edge( edgeid , _from , _to )
                        Edges.append(e)

                #self.maxroads = len (Edges)
                #self.maxjunctions = len (Nodes)

        def distance(self):             
                #go through every edge
                for e in Edges:
                    for node in Nodes:
                        #define start nodeand get coordinates
                        if node.nodeid == e.fromnode:
                            startnode = node
                            startnodex = int(node.xcord)
                            startnodey = int(node.ycord)
                        #define end node and get coordinates
                        if node.nodeid == e.tonode:
                            endnode = node
                            endnodex = int(node.xcord)
                            endnodey = int(node.ycord)
                    #distance calculation = (dx^2 + dy^2)^1/2
                    EdgeDistance = ((abs(startnodex - endnodex))**2 + (abs(startnodey - endnodey))**2 )**0.5
                    #start node, end node, distance, add to dictionary
                    d = dist(startnode, endnode, EdgeDistance)
                    Distances.append(d)

        def route(self):



            #the eventual desired route will need to be in the format: <route id="route01" edges="S1 E2 S5 E7 S9 S12"/> 
            # The Objective of this part is to the build the "S1 E2 S5 E7 S9 S12" part.
            for n in Nodes:
                NodesAvailable = Nodes[:]                                                                              #reset available nodes
                if n.nodeid.startswith('A'):                                                                        #make sure only correct nodes can be start nodes
                    startnode = n.nodeid                                                                            #set as startnode
                    NodesAvailable.remove(startnode)                                                                #remove startnode from future selections                 
                    for e in Edges:                                                                                 #go through available edges  
                        EdgesAvailable = Edges[:]                      
                        if e.fromnode == startnode:                                                                 #if applicable edge                            
                            nodeused = startnode                                                                    #define a new node
                            edgepicked = e.edgeid                                                                   #select the edge
                            EdgesAvailabe.remove(edgepicked)                                                        #remove the edge from future selections
                            while nodeused in NodesAvailable and nodeused.startswith('A') == False:                 #loop for while nodes available and not an end node
                                for ea in EdgesAvailable:                                                           #for edges left
                                    if ea.fromnode == nodeused:                                                     #that are reachable from current node
                                        nodeused = ea.tonode                                                        #select new node
                                        edgepicked = e.edgeid                                                       #select new edge
                                        NodesAvailable.remove(nodeused)                                                     #remove node from future selections
                                        CurrentRoute.append(edgepicked)                                                     #build a list
                                r = routeplan(edgepicked)
                                Routes.append(r)

                            



if ( __name__ == "__main__"):

	# create an XMLReader
	parser = xml.sax.make_parser()
	# turn off namespaces
	parser.setFeature(xml.sax.handler.feature_namespaces, 0)

	# override the default ContextHandler
	Handler = calc()
	parser.setContentHandler( Handler )

	parser.parse("xjunction.edg.xml")
	parser.parse("xjunction.nod.xml")

	for e in Edges:
		print e

	for n in Nodes:
		print n

	#  for e in Edges:
	#     print e.xml()

	#  for n in Nodes:
	#     print n.xml()

	Handler.distance()

	#if (Handler.maxroads != 0):
	#	print Handler.maxroads 
	#else:
	#	print "No max road"

	#if (Handler.maxjunctions != 0):
	#	print Handler.maxjunctions 
	#else:
	#	print "No max junction"

	for d in Distances:
		print d

	Handler.route()

	for r in Routes:
		print r
